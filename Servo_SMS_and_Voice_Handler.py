from gtts import gTTS
import africastalking
import logging
import os
import RPi.GPIO as GPIO
import time

class SSVH(object):

    def __init__(self,authname):
        self.apikey = "983a36a35b591a83c382ebe9a1a33e7ee3a5e0e98981e6f5d47da8e87f176798"
        self.username = "sandbox"
        self.intruder_msg = "Intruder Alert!"
        self.greetings = ["Goodmorning","Good afternoon","Good evening"]
        self.footer = "welcome home"
        self.intruder_voice = "Please wait as we confirm your identity"
        self.authname = authname
        GPIO.setmode(GPIO.BCM)
        self.control_pins = [6,13,19,26]
        for pin in self.control_pins:
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, False)
        door_open = [ [1,0,0,0], [1,1,0,0], [0,1,0,0], [0,1,1,0], [0,0,1,0], [0,0,1,1], [0,0,0,1], [1,0,0,1]]
        door_close = [ [1,0,0,1], [0,0,0,1], [0,0,1,1], [0,0,1,0], [0,1,1,0], [0,1,0,0], [1,1,0,0], [1,0,0,0] ]
        self.door_status = [door_open , door_close]

    def sendsms(self):

        africastalking.initialize(self.username, self.apikey)
        message_recipient = ['+254705066102']
        sms = africastalking.SMS
        smsresponse = sms.send(self.intruder_msg, message_recipient)
        logging.info(smsresponse)

    def text_to_speech(self):
        currentTime = int(time.strftime('%H'))
        time_selector = int(currentTime/6)
        if (time_selector == 0):
            time_selector = 1
        header = self.greetings[time_selector - 1]
        spoken_string = header +" "+ self.authname + " " + self.footer
        print(self.authname)
        if (self.authname == "Unknown") :
            print("intruder")
            audio = self.intruder_voice
            SSVH.sendsms(self)
        else:
            audio = spoken_string

        #text to speech conversion
        file = "test1.mp3"
        #initialize tts, create mp3 and play
        tts = gTTS(audio, 'en')
        tts.save(file)
        os.system("mpg123 " + file)

    def servo_control(self):
        for door_status in range(len(self.door_status)):
            for i in range(112):
              for halfstep in range(8):
                for pin in range(4):
                  GPIO.output(self.control_pins[pin], self.door_status[door_status][halfstep][pin])
                time.sleep(0.001)
            time.sleep(10)
        GPIO.cleanup()



